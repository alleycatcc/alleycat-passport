import _slicedToArray from "@babel/runtime/helpers/slicedToArray";
import { pipe, compose, composeRight, sprintf1, lets } from 'stick-js/es';
import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import express from 'express';
import { get, listen, use } from 'alleycat-js/es/express';
import { main as initExpressJwt, bufferEqualsConstantTime as bufferEquals, hashPasswordScrypt as _hashPasswordScrypt, secureMethod } from '../../../jslib/index.mjs';
var secureGet = secureMethod('get');
// const securePost = secureMethod ('post')
// ...

var port = 4444;
var JWT_SECRET = 'W@#$*nxnvxcv9f21jn13!!**j123n,mns,dg;';
var COOKIE_SECRET = 'j248idvnxcNNj;;;091!@#%***';
var PASSWORD_SALT = Buffer.from([0x75, 0x12, 0x23, 0x91, 0xAA, 0xAF, 0x53, 0x88, 0x90, 0xF1, 0xD4, 0xDD]);
var PASSWORD_KEYLEN = 64;
var hashPasswordScrypt = _hashPasswordScrypt(PASSWORD_SALT, PASSWORD_KEYLEN);
var getCredentials = function getCredentials() {
  return new Map([['allen@alleycat.cc', ['Allen', 'Haim', hashPasswordScrypt('appel')]], ['arie@alleycat.cc', ['arie', 'bombarie', hashPasswordScrypt('peer')]]]);
};
var credentials = getCredentials();
var loggedIn = new Set();
var getUser = function getUser(email) {
  var user = credentials.get(email);
  if (!user) return null;
  var _user2 = _slicedToArray(user, 3),
    firstName = _user2[0],
    lastName = _user2[1],
    password = _user2[2];
  return {
    password: password,
    userinfo: {
      email: email,
      firstName: firstName,
      lastName: lastName
    }
  };
};

// --- (String, Buffer) => Boolean
var checkPassword = function checkPassword(testPlain, knownHashed) {
  return lets(function () {
    return hashPasswordScrypt(testPlain);
  }, function (testHashed) {
    return pipe(testHashed, bufferEquals(knownHashed));
  });
};
var _initExpressJwt = initExpressJwt({
    checkPassword: checkPassword,
    getUser: getUser,
    isLoggedIn: function isLoggedIn(email) {
      return loggedIn.has(email);
    },
    jwtSecret: JWT_SECRET,
    onLogin: function onLogin(email, _user) {
      return loggedIn.add(email);
    },
    onLogout: function onLogout(email, done) {
      if (loggedIn["delete"](email)) return done(null);
      return done('Unexpected, ' + email + ' not found in `loggedIn`');
    },
    usernameField: 'email'
  }),
  addLoginMiddleware = _initExpressJwt.addMiddleware;
var go = function go() {
  pipe(pipe(pipe(pipe(pipe(pipe(express(), use(bodyParser.json())), use(cookieParser(COOKIE_SECRET))), addLoginMiddleware) // --- only available to logged-in users
  , secureGet('/data', function (_req, res) {
    var event = new Date();
    var data = event.toLocaleTimeString('nl-NL');
    res.send({
      data: data
    });
  })) // --- always available
  , get('/data-public', function (_req, res) {
    var event = new Date();
    var data = event.toLocaleTimeString('nl-NL');
    res.send({
      data: data
    });
  })), listen(port, function () {
    console.log(pipe(port, sprintf1('App listening on port %s')));
  }));
};
go();