import { pipe, compose, composeRight, recurry, ifPredicateResults } from 'stick-js/es';

// :: a -> Map a b -> b | undefined
export var mapHas = recurry(2)(function (k) {
  return function (m) {
    return m.has(k) ? m.get(k) : void 8;
  };
});

// :: a -> (b -> c) -> (() -> d) -> Map a b -> c | d
export var ifMapHas = recurry(4)(composeRight(mapHas, ifPredicateResults));