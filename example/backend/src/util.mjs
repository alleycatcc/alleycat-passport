import {
  pipe, compose, composeRight,
  recurry, ifPredicateResults,
} from 'stick-js/es'

// :: a -> Map a b -> b | undefined
export const mapHas = recurry (2) (
  (k) => (m) => m.has (k) ? m.get (k) : void 8
)

// :: a -> (b -> c) -> (() -> d) -> Map a b -> c | d
export const ifMapHas = recurry (4) (
  mapHas >> ifPredicateResults,
)
