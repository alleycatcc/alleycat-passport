import {
  pipe, compose, composeRight,
  sprintf1, lets,
} from 'stick-js/es'

import bodyParser from 'body-parser'
import cookieParser from 'cookie-parser'
import express from 'express'

import { get, listen, use, } from 'alleycat-js/es/express'

import {
  main as initExpressJwt,
  bufferEqualsConstantTime as bufferEquals,
  hashPasswordScrypt as _hashPasswordScrypt,
  secureMethod,
} from '../../../jslib/index.mjs'

const secureGet = secureMethod ('get')
// const securePost = secureMethod ('post')
// ...

const port = 4444
const JWT_SECRET = 'W@#$*nxnvxcv9f21jn13!!**j123n,mns,dg;'
const COOKIE_SECRET = 'j248idvnxcNNj;;;091!@#%***'
const PASSWORD_SALT = Buffer.from ([
  0x75, 0x12, 0x23, 0x91, 0xAA, 0xAF, 0x53, 0x88, 0x90, 0xF1, 0xD4, 0xDD,
])
const PASSWORD_KEYLEN = 64

const hashPasswordScrypt = _hashPasswordScrypt (PASSWORD_SALT, PASSWORD_KEYLEN)

const getCredentials = () => new Map ([
  ['allen@alleycat.cc', ['Allen', 'Haim', hashPasswordScrypt ('appel')]],
  ['arie@alleycat.cc', ['arie', 'bombarie', hashPasswordScrypt ('peer')]],
])
const credentials = getCredentials ()
const loggedIn = new Set ()

const getUser = (email) => {
  const user = credentials.get (email)
  if (!user) return null
  const [firstName, lastName, password] = user
  return {
    password,
    userinfo: {
      email,
      firstName,
      lastName,
    },
  }
}

// --- (String, Buffer) => Boolean
const checkPassword = (testPlain, knownHashed) => lets (
  () => hashPasswordScrypt (testPlain),
  (testHashed) => testHashed | bufferEquals (knownHashed),
)

const { addMiddleware: addLoginMiddleware, } = initExpressJwt ({
  checkPassword,
  getUser,
  isLoggedIn: (email) => loggedIn.has (email),
  jwtSecret: JWT_SECRET,
  onLogin: (email, _user) => loggedIn.add (email),
  onLogout: (email, done) => {
    if (loggedIn.delete (email)) return done (null)
    return done ('Unexpected, ' + email + ' not found in `loggedIn`')
  },
  usernameField: 'email',
})

const go = () => {
  express ()
    | use (bodyParser.json ())
    | use (cookieParser (COOKIE_SECRET))
    | addLoginMiddleware
    // --- only available to logged-in users
    | secureGet ('/data', (_req, res) => {
      const event = new Date ()
      const data = event.toLocaleTimeString ('nl-NL')
      res.send ({ data, })
    })
    // --- always available
    | get ('/data-public', (_req, res) => {
      const event = new Date ()
      const data = event.toLocaleTimeString ('nl-NL')
      res.send ({ data, })
    })
    | listen (port, () => {
      console.log (port | sprintf1 ('App listening on port %s'))
    })
}

go ()
