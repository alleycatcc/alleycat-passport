import _objectWithoutProperties from "@babel/runtime/helpers/objectWithoutProperties";
var _excluded = ["userinfo"];
import { pipe, compose, composeRight, ifPredicateResults, noop, nil } from 'stick-js/es';
import jwtModule from 'jsonwebtoken';
import passport from 'passport';
import localStrategy from 'passport-local';
import { Strategy as JWTStrategy } from 'passport-jwt';
import { getN, post, postN, send, sendStatus, methodWithMiddlewares, methodNWithMiddlewares, method3WithMiddlewares } from 'alleycat-js/es/express';
import { composeManyRight, logWith } from 'alleycat-js/es/general';
import { warn } from 'alleycat-js/es/io';
import { bufferEqualsConstantTime, hashPasswordScrypt } from './util-crypt.mjs';
export { bufferEqualsConstantTime, hashPasswordScrypt };
var jwtMiddleware = passport.authenticate('jwt', {
  session: false
});
export var secureMethodN = methodNWithMiddlewares([jwtMiddleware]);
export var secureMethod = methodWithMiddlewares([jwtMiddleware]);
export var secureMethod3 = method3WithMiddlewares([jwtMiddleware]);

// --- must return String or null
var jwtFromSignedCookie = function jwtFromSignedCookie(req) {
  var _req$signedCookies$jw;
  return (_req$signedCookies$jw = req.signedCookies.jwt) !== null && _req$signedCookies$jw !== void 0 ? _req$signedCookies$jw : null;
};
var getCookieOptions = function getCookieOptions() {
  var secure = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : true;
  return {
    secure: secure,
    httpOnly: true,
    sameSite: true,
    signed: true
  };
};
var initStrategies = function initStrategies(_ref) {
  var jwtSecret = _ref.jwtSecret,
    ifLoggedInUserinfo = _ref.ifLoggedInUserinfo,
    getUser = _ref.getUser,
    checkPassword = _ref.checkPassword,
    usernameField = _ref.usernameField,
    passwordField = _ref.passwordField;
  passport.use('login', new localStrategy(
  // --- on failure to retrieve these, this will result in roughly
  //   `done (null, null, { message: 'Missing credentials', })`
  {
    usernameField: usernameField,
    passwordField: passwordField
  }, function (username, password, done) {
    var user = getUser(username);
    if (!user) return done(null, false, {
      message: 'User not found'
    });
    var passwordFromDb = user.password,
      userinfo = user.userinfo;
    if (!passwordFromDb || !userinfo) return done('Invalid user object', false, {
      message: 'Internal error'
    });
    if (!checkPassword(password, passwordFromDb)) return done(null, false, {
      message: 'Wrong Password'
    });
    return done(null, {
      username: username,
      userinfo: userinfo
    }, {
      message: 'logged in successfully'
    });
  }));
  passport.use('jwt', new JWTStrategy({
    jwtFromRequest: jwtFromSignedCookie,
    secretOrKey: jwtSecret
  },
  // --- once we're here, it means that the JWT was valid and we were able to decode it.
  function (_ref2, done) {
    var username = _ref2.username;
    return done(null, pipe(username, ifLoggedInUserinfo(
    // --- 200, user object is now available as `req.user`
    function (userinfo) {
      return {
        username: username,
        userinfo: userinfo
      };
    },
    // --- 401
    function () {
      return false;
    })));
  }));
};

/*
 * `getUser` must return { password, userinfo, }, where userinfo is an
 * arbitrary structure, or `null` if the username is invalid.
 *
 * This structure will be sent to the frontend in the response to the both
 * the /hello and /login routes.
 *
 * The structure { username, userinfo, } will be made available on each
 * request as `req.user` after the JWT is successfully decoded.
 *
 * Note that this is not what is stored in the JWT, which is currently
 * only the username.
 *
 * `checkPassword` :: (String, Buffer) -> Boolean
 */

export var main = function main(_ref3) {
  var checkPassword = _ref3.checkPassword,
    getUser = _ref3.getUser,
    isLoggedIn = _ref3.isLoggedIn,
    jwtSecret = _ref3.jwtSecret,
    _ref3$onLogin = _ref3.onLogin,
    onLogin = _ref3$onLogin === void 0 ? noop : _ref3$onLogin,
    _ref3$onLogout = _ref3.onLogout,
    onLogout = _ref3$onLogout === void 0 ? noop : _ref3$onLogout,
    _ref3$routeHello = _ref3.routeHello,
    routeHello = _ref3$routeHello === void 0 ? '/hello' : _ref3$routeHello,
    _ref3$routeLogin = _ref3.routeLogin,
    routeLogin = _ref3$routeLogin === void 0 ? '/login' : _ref3$routeLogin,
    _ref3$routeLogout = _ref3.routeLogout,
    routeLogout = _ref3$routeLogout === void 0 ? '/logout' : _ref3$routeLogout,
    _ref3$usernameField = _ref3.usernameField,
    usernameField = _ref3$usernameField === void 0 ? 'username' : _ref3$usernameField,
    _ref3$passwordField = _ref3.passwordField,
    passwordField = _ref3$passwordField === void 0 ? 'password' : _ref3$passwordField;
  var getUserinfo = function getUserinfo(email) {
    if (!isLoggedIn(email)) return false;
    var user = getUser(email);
    if (nil(user)) return warn('Unexpected, unable to get user info');
    return user.userinfo;
  };
  var ifLoggedInUserinfo = pipe(getUserinfo, ifPredicateResults);
  initStrategies({
    jwtSecret: jwtSecret,
    ifLoggedInUserinfo: ifLoggedInUserinfo,
    getUser: getUser,
    checkPassword: checkPassword,
    usernameField: usernameField,
    passwordField: passwordField
  });
  var addMiddleware = composeManyRight(
  // --- all routes with the passport 'jwt' middlreturns return 401 if either the JWT is missing
  // or invalid, or if the user inside the JWT is not logged in, and 200 if the user is logged in.
  getN(routeHello, [passport.authenticate('jwt', {
    session: false
  }), function (req, res) {
    var user = req.user;
    if (!user) return pipe(res, sendStatus(500, {
      imsg: routeHello + ': missing user info'
    }));
    var userinfo = user.userinfo,
      _ = _objectWithoutProperties(user, _excluded);
    return pipe(res, send({
      data: userinfo
    }));
  }]), post(routeLogin, function (req, res) {
    var cookieOptions = getCookieOptions(req.secure);
    res.clearCookie('jwt', cookieOptions);
    // --- note, automatically calls req.login, a passort function (see
    // https://www.passportjs.org/concepts/authentication/login); we don't
    // call req.logout.
    passport.authenticate('login', function (err, user) {
      var _ref4 = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {},
        message = _ref4.message;
      if (err) {
        warn('Error with login:', err);
        return pipe(res, sendStatus(500, {
          imsg: null,
          umsg: 'Server error during login (see logs)'
        }));
      }
      if (!user) return pipe(res, sendStatus(401, {
        umsg: 'Invalid login: ' + message
      }));
      var username = user.username,
        userinfo = user.userinfo;
      var jwt = jwtModule.sign({
        username: username
      }, jwtSecret);
      res.cookie('jwt', jwt, cookieOptions);
      onLogin(username, user);
      return pipe(res, send({
        data: userinfo
      }));
    })(req, res);
  }), postN(routeLogout, [passport.authenticate('jwt', {
    session: false
  }), function (req, res) {
    res.clearCookie('jwt', getCookieOptions(req.secure));
    var username = req.user.username;
    if (!username) return pipe(res, sendStatus(500, {
      imsg: 'req.user.username was empty'
    }));
    onLogout(username, function (err) {
      if (err) return pipe(res, sendStatus(500, {
        imsg: 'onLogout: ' + err
      }));
    });
    return pipe(res, send({}));
  }]));
  return {
    addMiddleware: addMiddleware
  };
};