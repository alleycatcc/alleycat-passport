### Run example

- Install deps

    root=$(pwd -P)
    git submodule update --init --recursive
    yarn # or npm i (not tested)

- Build and run backend
    
    bin/build-example-backend
    node lib/index.mjs &

- Prepare frontend

    cd "$root"/example/frontend
    source ../../node_modules/alleycat-frontend/bash-aliases

- Run frontend dev server or build frontend

    run-dev-server --port=xxx
    # or build-tst

- Browse to http://localhost:xxx
